# Daniel's Doodle!

## Setup

```
npm install
```

## Development

```
npm run start
```

## Build & Deploy

```
npm build
```

Deploys are automatic - just commit to the `master` branch! A minute or two later, the site will be live here: https://daniels-doodle.netlify.app/
