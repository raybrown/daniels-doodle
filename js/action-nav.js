import anime from 'animejs/lib/anime.es'
import actions from './actions'
import { supports } from './supports'

class ActionNav {
  constructor($elem = document.querySelector('nav')) {
    if (! $elem) return

    this.$root = $elem;
    this.$buttons = $elem.querySelectorAll('button')
    this.deselectEvent = supports.touch() ? 'touchend' : 'mouseup'
  }

  init() {
    document.addEventListener(this.deselectEvent, this.deselect.bind(this))
    document.addEventListener('contextmenu', this.deselect.bind(this))
  }

  createAnimation($button, action) {
    const opts = actions[action];
    const actionIsTimeline = typeof opts === 'object' && opts.length

    if (actionIsTimeline) {
      $button.animation = anime.timeline()

      for (let i = 0; i < opts.length; i++) {
        $button.animation.add(opts[i], 0);
      }
    } else {
      $button.animation = anime(opts)
    }
  }

  deselect(event) {
    if (! event.target.classList.contains('action-nav__button')) return

    const $button = event.target

    if (! $button) return

    const action = $button.value

    if (event.type == this.deselectEvent) {
      if ($button.animation) {
        $button.animation.restart()
      } else {
        this.createAnimation($button, action)
      }
    }
  }
}

export default ActionNav
