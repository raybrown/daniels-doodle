import { supports } from './supports'

class Button {
  constructor($elem) {
    if (typeof $elem === undefined) return

    this.$elem = $elem;
    this.deselectEvent = supports.touch() ? 'touchend' : 'mouseup'
    this.selectEvent = supports.touch() ? 'touchstart' : 'mousedown'
  }

  init() {
    this.$elem.addEventListener(this.selectEvent, this.select.bind(this))
    this.$elem.addEventListener(this.deselectEvent, this.deselect.bind(this))
    this.$elem.addEventListener('contextmenu', this.deselect.bind(this))
  }

  deselect() {
    if (this.$elem.classList.contains('active')) {
      this.$elem.classList.remove('active')
    }
  }

  select() {
    this.$elem.classList.add('active')
  }
}

export default Button
