import { testSupports } from './supports'
import ActionNav from './action-nav'
import Button from './button'
import Room from './room'

document.addEventListener("DOMContentLoaded", function(event) {
  testSupports()

  const nav = new ActionNav().init()
  const room = new Room().init()

  const buttons = document.querySelectorAll('[type="button"]')
  for (let i = 0; i < buttons.length; i++) {
    new Button(buttons[i]).init()
  }
});
