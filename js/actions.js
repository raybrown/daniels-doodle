export default {
  blink: {
    targets: '.doodle__eye',
    scaleY: 0,
    duration: 150,
    direction: 'alternate',
  },

  jump: [
    {
      targets: '.doodle',
      translateY: [0, -50, 0],
      duration: 1000,
      easing: 'easeInOutCirc',
    },

    {
      targets: '.doodle__left-arm',
      rotate: [-20, 20, 0],
      duration: 1000,
      direction: 'alternate',
      easing: 'easeInOutSine',
    },

    {
      targets: '.doodle__right-arm',
      rotate: [20, -20, 0],
      duration: 1000,
      direction: 'alternate',
      easing: 'easeInOutSine',
    }
  ],

  spin: {
    targets: '.doodle__propeller',
    rotateY: 360 * 2,
    duration: 1000,
    easing: 'easeInOutCirc',
  },

  wave: {
    targets: '.doodle__right-arm',
    rotate: 20,
    direction: 'alternate',
    loop: 6,
    duration: 100,
    easing: 'easeInOutSine'
  },

  wiggle: {
    targets: '.doodle__bowtie',
    rotate: 10,
    direction: 'alternate',
    loop: 8,
    duration: 100,
    easing: 'easeInOutSine'
  },

  wink: {
    targets: '.doodle__left-eye',
    scaleY: 0,
    duration: 200,
    direction: 'alternate',
  },
}
