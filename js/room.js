class Room {
  constructor() {
    this.$elem = document.querySelector('body')
    this.$colorSlider = document.querySelector('.room__nav__color-slider')
    this.$lightSwitch = document.querySelector('.room__nav__light-switch')
    this.$nightLight = document.querySelector('.room__night-light')
    this.lights = true
  }

  init() {
    this.$elem.classList.add('room')
    this.$elem.classList.add('room--lights-' + (this.lights ? 'on' : 'off'))

    this.$lightSwitch.addEventListener('click', this.toggleLights.bind(this))
    this.$colorSlider-addEventListener('input', this.adjustHue.bind(this))
  }

  adjustHue() {
    let value = this.$colorSlider.value

    this.$nightLight.style.filter = `hue-rotate(${ value }deg)`
  }

  toggleLights() {
    (this.lights) ? this.turnLightsOff() : this.turnLightsOn()
  }

  turnLightsOn() {
    this.lights = true
    this.$elem.classList.add('room--lights-on')
    this.$elem.classList.remove('room--lights-off')
  }

  turnLightsOff() {
    this.lights = false
    this.$elem.classList.add('room--lights-off')
    this.$elem.classList.remove('room--lights-on')
  }
}

export default Room
