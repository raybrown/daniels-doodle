const supports = {
  js() {
    return true
  },

  touch() {
    return 'ontouchstart' in document.documentElement
  }
}

function testSupports() {
  document._supports = supports;

  for (const key in supports) {
    const $html = document.querySelector('html')
    let supportedClass = key
    let unsupportedClass = `no-${key}`

    if (supports[key]()) {
      $html.classList.add(supportedClass)
      $html.classList.remove(unsupportedClass)
    } else {
      $html.classList.add(unsupportedClass)
      $html.classList.remove(supportedClass)
    }
  }
}


export {
  supports,
  testSupports
}
